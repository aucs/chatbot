#!/usr/bin/env ruby
# chatbot.rb
# Main file for the chatbot script.

$DIR = File.dirname(__FILE__);
$LOAD_PATH.push("#{$DIR}/lib");

require 'chatbot';

mode = :default;

# Determine which mode we want to start in.
$args = [];
ARGV.each do |argument|
	case argument
	when "--server"
		mode = :server;
	when "--default"
		mode = :default;
	when "--debug"
		mode = :debug;
	else
		$args << argument;
	end
end
ARGV.clear;

case mode
when :debug
	require 'debug/chatbot';

	$chatbot = Chatbot.new;

	$args.each{ |file|
		load "debug/#{file}.rb";
	};

	loop do
		print "(debug) : ";
		input = gets;
		break if input.nil? or input.strip.empty?;
		$chatbot.cmd(input);
	end
when :default
	chatbot = Chatbot.new;

	loop do
		print "you > ";
		input = gets;
		break if input.nil? or input.strip.empty?;
		chatbot.ask(input, ->(reply){
			puts "chatbot > #{reply}";
		});
	end
when :server
	require 'server';

	server = Chatbot::Server.new;
end
