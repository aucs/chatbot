require 'composer';
require 'component';
require 'timeout';

class Chatbot
	attr_reader :components;

	@@default_components = [ 'generic', 'fallback', 'marcopolo', 'clock' ];
	@@default_composer = Composer;

	def ask(message, fallback=->{})
		send(message, fallback);
	end

	def send(message, fallback=->{}, wait:1.0)
		# Send the same message to all of our components.
		@composer.start;

		# Send the reply to the previous component.
		last_used_component = @composer.used_components.last;
		if @components.key? last_used_component
			@components[last_used_component].replied(message.chomp, @composer.replies.last);
		end

		begin
			threads = [];

			Timeout::timeout(wait) {
				# Send every component the message to process.
				@components.each{ |_, m|
					threads << Thread.new{
						m.message(message.chomp);
					}
				};

				threads.each{ |thread|
					thread.join;
				}
			}
		rescue Timeout::Error
			puts "Timed out...";

			threads.each{ |thread|
				thread.kill;
			}
		end

		# All components have replied.
		reply = @composer.finish;
		fallback.(reply);
	end

	private

	def load_composer(composer)
		@composer = composer.new;
	end

	def load_components(*components)
		components.each{ |m|
			@components[m] = Component.load(m);
			@components[m].composer = @composer;
		};
	end

	def initialize
		@components = {};

		@history = [];

		load_composer(@@default_composer);
		load_components(*@@default_components);
	end
end
