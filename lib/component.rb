require "component_base";

class Component < ComponentBase
	# self.load loads instances of components and returns them.
	def self.load(name)
		require "component/#{name}";
		o = eval name.capitalize;
		puts "Loaded component '#{name}' as #{o}...";

		mod = o.new;
		mod.id = name;
		return mod;
	end

	public

	# Called when this component receives a message.
	# Override this function!
	def receive(message, previous_message)

	end

	# Called when the user replied to a message sent by this component.
	def replied(message, previous_messsage)

	end
end
