# Time now, for now

class Component::Clock < Component
	def receive(message)
		msg = message.downcase
		if ["time", "now"].all? {|key_word| msg.include? key_word }
			t = Time.now
			reply("It's #{t.strftime('%l:%M%p').strip}", confidence = 0.5);
		end
	end
end
