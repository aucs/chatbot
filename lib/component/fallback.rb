# This component will simply state that the chatbot does not understand the
# question.

class Component::Fallback < Component
	def receive(message)
		reply([
			"I don't understand.",
			"I am not sure what you mean.",
			"What is that?",
			"What do you mean?",
			"I do not know.",
		].sample, 0.01);
	end
end
