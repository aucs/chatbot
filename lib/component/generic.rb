# This is a generic question and answer component.
# It works quite well with fuzzy questions, given the knowledgebase is large
# enough.

class Component::Generic < Component
	def receive(message)
		answers = get_answers(message);
		reply(answers.sample, 0.22) if !answers.empty?;
	end

	def replied(message, previous_message)
		# Learn that you can reply "message" to "previous_message"
		learn(previous_message, message);
	end

	def learn(question, answer)
		set_answers(question, answer);
		insert_answers(@data, question, answer);
	end

	private

	def get_answers(message)
		# Go through all our q->a data, from more specific to less specific.

		# 1. Case insensitive search.
		message = message.downcase;
		if @qa_lowercase.key? message;
			puts "Used case insensitive search";
			return @qa_lowercase[message];
		end

		# 2. No punctuation search.
		message = message.gsub(/[^\w\s]/,"");
		if @qa_no_punctuation.key? message;
			puts "Used no punctuation search";
			return @qa_no_punctuation[message];
		end

		# 3. Word by word search.
		# "What is the size of the sun?" matches "What is the size ... ...", and
		# hopefully the answer will be close enough.
		message = message.split(/\s/);

		# Loop backwards starting from the full sentence, removing words from the
		# end until we find a match or run out of words.
		(0...message.size).each{ |i|
			question = message[0..(message.size-i-1)].join;
			if @qa_word_by_word.key? question;
				puts "Used word by word search";
				return @qa_word_by_word[question];
			end
		};

		return [];
	end

	def set_answers(message, *answers)
		message = message.downcase;
		insert_answers(@qa_lowercase, message, *answers);

		message = message.gsub(/[^\w\s]/,"");
		insert_answers(@qa_no_punctuation, message, *answers);

		message = message.split(/\s/);
		(0...message.size).each{ |i|
			question = message[0..(message.size-i-1)].join;
			insert_answers(@qa_word_by_word, question, *answers);
		};
	end

	def insert_answers(hash, key, *answers)
		hash[key] = [] if !hash.key? key;
		hash[key] += answers;
	end

	def generate_key(message)
		key = message.strip;
		key = key.gsub(/\W/,"").downcase;
		return key;
	end

	def initialize()
		@qa_lowercase = {};
		@qa_no_punctuation = {};
		@qa_word_by_word = {};

		# A simple dictionary containing messages in question->[answers] format.
		@data = data("generic/conversation");
		puts "Initializing generic q/a data...";
		@data.each{ |question, answers|
			set_answers(question, *answers);
		};
	end
end
