# This component replies "Hello!" to everything.

class Component::Hello < Component
	def receive(message)
		reply(["Hello!","Hi!"].sample);
	end
end
