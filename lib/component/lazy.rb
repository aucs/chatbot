# This component takes really long to answer.

class Component::Lazy < Component
	def receive(message)
		sleep 5;
		reply("...what?");
	end
end
