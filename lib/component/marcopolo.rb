# Marco! Polo!

class Component::Marcopolo < Component
	def receive(message)
		if message.downcase.include? "marco"
			reply("Polo!", confidence = 1.0);
		end
	end
end
