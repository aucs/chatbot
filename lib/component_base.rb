require "datahandler";

class ComponentBase
	attr_accessor :composer;
	attr_accessor :id;

	public

	# Called when a new message is sent by the user.
	def message(message)
		receive(message);
		ready;
	end

	def on_ready(pr)
		@on_ready = pr;
	end

	private

	# Called if a component needs some additional data.
	def data(id)
		DataHandler.new(id);
	end

	# Called when the component is ready.
	def ready
		@on_ready.() if !@on_ready.nil?;
		@on_ready = nil;
	end

	# Called when the component wants to reply to a message.
	def reply(message, confidence=0.01)
		@composer.confidence(@id, confidence);
		@composer.reply(@id, message);
		ready;
	end
end
