class Composer
	attr_reader :used_components, :replies;

	def start
		@messages = {};
		@confidences = {};
	end

	def finish()
		return nil if @messages.empty?;

		# Get the most confident response.
		best_component = @confidences.to_a.sort{ |a, b| b[1] - a[1] }.first[0];

		reply = @messages[best_component];

		puts "#{@confidences}";
		@used_components << best_component;
		@replies << reply;

		return reply;
	end

	def confidence(id, value)
		@confidences[id] = value;
	end

	def reply(id, value)
		@messages[id] = value;
	end

	private

	def initialize
		# Keeps track of components that were used in the past.
		@used_components = [];
		# Keeps track of messages we sent in the past.
		@replies = [];
	end
end
