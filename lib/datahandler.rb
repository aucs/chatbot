# Handles the data transfer to components and back.
# Abstracts away the data backend we end up using, be it a database or simple
# JSON files.

require "json";
require "fileutils";

class DataHandler < Hash
	def load_from_file(file)
		return if !File.exist?(file);
		puts "Loading data from #{file}...";
		data = File.read(file);
		begin
			self.merge!(JSON.parse(data));
		rescue JSON::ParserError
			puts "File #{file} seems corrupt! D:";
			puts "I have saved it to #{file}.bak for backup.";
			puts "Please take a look at the file to make sure no data is lost.";
			FileUtils.cp("#{file}", "#{file}.bak");
		end
	end

	def save_to_file(file)
		puts "Saving data to #{file}...";
		# Make subdirectories if necessary.
		dir=File.dirname(file);
		Dir.mkdir(dir) if !Dir.exist?(dir);
		File.write(file, self.to_json);
	end

	private

	def initialize(id)
		@id = id;

		filename = "#{$DIR}/data/#{id}.json";

		load_from_file(filename);

		at_exit{
			save_to_file(filename);
		}
	end
end
