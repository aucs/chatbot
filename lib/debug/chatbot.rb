puts "[1m CHATBOT IS RUNNING IN DEBUG MODE [m";

module Chatbot::Debug
	def cmd(string)
		string = string.split;
		command = string.shift;
		arguments = string;
		case command
		when "help"
			puts "I don't think I can help you :("
		when "debug" # run a file from debug/[file].rb
			file = arguments.shift;
			load "debug/#{file}.rb";
		when "say" # say something to the chatbot
			ask(arguments.join(" "), ->(reply){
				puts "chatbot > #{reply}";
			});
		else
			puts "Unknown command: #{command}";
		end
	end
end

class Chatbot
	include Chatbot::Debug
end
