# Used to easily fill a generic's q->a data storage.

print "generic Q > ";
question = gets;
exit if question.nil?;
question = question.chomp;

while true
	while true
		puts "< Q: #{question}";
		print "generic A > ";
		input = gets;
		exit if input.nil?;
		input = input.chomp;
		break if input.strip.empty?;

		answer = input;
		puts "#{question} -> #{answer}";
		$chatbot.components["generic"].learn(question, answer);
	end

	break if question == answer;
	question = answer;
end
