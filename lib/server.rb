require 'socket';
require 'json';

class Chatbot::Server
	def initialize(host: 'localhost', port: 2222)
		@server = TCPServer.new(host, port);
		@bot = Chatbot.new;

		puts "Listening on #{port}...";

		loop do
			socket = @server.accept;

			begin
				content_length = 0;

				puts socket.gets

				while true
					header = socket.gets
					if !header[/content-length/i].nil?
						content_length = header[/\d+/].to_i;
					end
					break if header.strip.empty?;
				end

				socket.print("HTTP/1.1 200\r\n");

				json = JSON.parse(socket.read(content_length));

				puts "client > #{json["msg"]}";

				@bot.ask(json["msg"], ->(reply){
					response = "#{reply}\n";

					socket.print("Content-Type: text/plain\r\n" +
											 "Content-Length: #{response.bytesize}\r\n" +
											 "Access-Control-Allow-Origin: null\r\n" +
											 "Connection: close\r\n");

					socket.print("\r\n");

					socket.print("#{response}");
				});
			rescue Exception => e
				puts "Exception: #{e}";

				socket.print("\r\n");

				socket.print("500\n");
			end

			socket.close;
		end
	end
end
